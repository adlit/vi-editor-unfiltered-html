# Editor Unfiltered HTML

This plugin allows Editors to add unfiltered HTML code to posts. For example: PayPal Button code.

Based on [this article](https://kellenmace.com/add-unfiltered_html-capability-to-admins-or-editors-in-wordpress-multisite/).

## Instructions:

Simply activate the plugin and this capability will be added to all Editors. 
