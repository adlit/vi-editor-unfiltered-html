<?php
/*
	Plugin Name: Vector & Ink Unfiltered HTML for Editors
	Description: Allows editors to add unfiltered HTML.
	Author: Daniel Braun, Vector & Ink.
	Version: 1.0.0
	Author URI: https://www.vectorandink.com/
*/

/**
 * Enable unfiltered_html capability for Editors.
 *
 * @param  array $caps The user's capabilities.
 * @param  string $cap Capability name.
 * @param  int $user_id The user ID.
 *
 * @return array  $caps    The user's capabilities, with 'unfiltered_html' potentially added.
 */
function vi_add_unfiltered_html_capability_to_editors( $caps, $cap, $user_id ) {
	if ( 'unfiltered_html' === $cap && user_can( $user_id, 'editor' ) ) {
		$caps = array( 'unfiltered_html' );
	}

	return $caps;
}

add_filter( 'map_meta_cap', 'vi_add_unfiltered_html_capability_to_editors', 1, 3 );
